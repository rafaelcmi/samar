require "rails_helper"

RSpec.describe AcoesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/acoes").to route_to("acoes#index")
    end

    it "routes to #new" do
      expect(:get => "/acoes/new").to route_to("acoes#new")
    end

    it "routes to #show" do
      expect(:get => "/acoes/1").to route_to("acoes#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/acoes/1/edit").to route_to("acoes#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/acoes").to route_to("acoes#create")
    end

    it "routes to #update" do
      expect(:put => "/acoes/1").to route_to("acoes#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/acoes/1").to route_to("acoes#destroy", :id => "1")
    end

  end
end
