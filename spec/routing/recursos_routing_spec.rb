require "rails_helper"

RSpec.describe RecursosController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/recursos").to route_to("recursos#index")
    end

    it "routes to #new" do
      expect(:get => "/recursos/new").to route_to("recursos#new")
    end

    it "routes to #show" do
      expect(:get => "/recursos/1").to route_to("recursos#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/recursos/1/edit").to route_to("recursos#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/recursos").to route_to("recursos#create")
    end

    it "routes to #update" do
      expect(:put => "/recursos/1").to route_to("recursos#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/recursos/1").to route_to("recursos#destroy", :id => "1")
    end

  end
end
