require "rails_helper"

RSpec.describe PerfisController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/perfis").to route_to("perfis#index")
    end

    it "routes to #new" do
      expect(:get => "/perfis/new").to route_to("perfis#new")
    end

    it "routes to #show" do
      expect(:get => "/perfis/1").to route_to("perfis#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/perfis/1/edit").to route_to("perfis#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/perfis").to route_to("perfis#create")
    end

    it "routes to #update" do
      expect(:put => "/perfis/1").to route_to("perfis#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/perfis/1").to route_to("perfis#destroy", :id => "1")
    end

  end
end
