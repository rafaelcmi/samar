require "rails_helper"

RSpec.describe ControladoresController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/controladores").to route_to("controladores#index")
    end

    it "routes to #new" do
      expect(:get => "/controladores/new").to route_to("controladores#new")
    end

    it "routes to #show" do
      expect(:get => "/controladores/1").to route_to("controladores#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/controladores/1/edit").to route_to("controladores#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/controladores").to route_to("controladores#create")
    end

    it "routes to #update" do
      expect(:put => "/controladores/1").to route_to("controladores#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/controladores/1").to route_to("controladores#destroy", :id => "1")
    end

  end
end
