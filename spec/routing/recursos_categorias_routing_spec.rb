require "rails_helper"

RSpec.describe RecursosCategoriasController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/recursos_categorias").to route_to("recursos_categorias#index")
    end

    it "routes to #new" do
      expect(:get => "/recursos_categorias/new").to route_to("recursos_categorias#new")
    end

    it "routes to #show" do
      expect(:get => "/recursos_categorias/1").to route_to("recursos_categorias#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/recursos_categorias/1/edit").to route_to("recursos_categorias#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/recursos_categorias").to route_to("recursos_categorias#create")
    end

    it "routes to #update" do
      expect(:put => "/recursos_categorias/1").to route_to("recursos_categorias#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/recursos_categorias/1").to route_to("recursos_categorias#destroy", :id => "1")
    end

  end
end
