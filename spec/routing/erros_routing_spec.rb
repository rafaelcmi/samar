require "rails_helper"

RSpec.describe ErrosController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/erros").to route_to("erros#index")
    end

    it "routes to #new" do
      expect(:get => "/erros/new").to route_to("erros#new")
    end

    it "routes to #show" do
      expect(:get => "/erros/1").to route_to("erros#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/erros/1/edit").to route_to("erros#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/erros").to route_to("erros#create")
    end

    it "routes to #update" do
      expect(:put => "/erros/1").to route_to("erros#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/erros/1").to route_to("erros#destroy", :id => "1")
    end

  end
end
