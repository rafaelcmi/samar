require "rails_helper"

RSpec.describe ProfissoesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/profissoes").to route_to("profissoes#index")
    end

    it "routes to #new" do
      expect(:get => "/profissoes/new").to route_to("profissoes#new")
    end

    it "routes to #show" do
      expect(:get => "/profissoes/1").to route_to("profissoes#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/profissoes/1/edit").to route_to("profissoes#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/profissoes").to route_to("profissoes#create")
    end

    it "routes to #update" do
      expect(:put => "/profissoes/1").to route_to("profissoes#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/profissoes/1").to route_to("profissoes#destroy", :id => "1")
    end

  end
end
