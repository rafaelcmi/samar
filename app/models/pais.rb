class Pais < ActiveRecord::Base
  include SearchModule
  
  UF = %w[AC AL AM AP BA CE DF ES GO MA MG MS MT PA PB PE PI PR RJ RN RO RR RS SC SE SP TO]

  # Relacionamentos
  has_many :telefone
end