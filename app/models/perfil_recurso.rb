class PerfilRecurso < ActiveRecord::Base
  self.table_name = 'perfis_recursos'

  # atributos virtuais ----------------------------------------------------------------------------
  attr_accessor :nome, :selecionado

  # relacionamentos -------------------------------------------------------------------------------
  belongs_to :perfil
  belongs_to :recurso

  # validações ------------------------------------------------------------------------------------
  validates :perfil_id, :recurso_id, presence: true
  validates :perfil_id, uniqueness: { scope: :recurso_id }
  validates :recurso_id, uniqueness: { scope: :perfil_id }
end