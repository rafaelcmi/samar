class Perfil < ActiveRecord::Base
  include SearchModule

  NOME_PERFIL_GESTOR = 'GESTOR'
  NOME_PERFIL_CANDIDATO = 'CANDIDATO'
  NOME_PERFIL_INSTRUTOR = 'INSTRUTOR'

  # relacionamentos -------------------------------------------------------------------------------
  has_many :participacao, dependent: :delete_all
  has_many :perfil_recurso, dependent: :delete_all
  has_many :pessoa, through: :participacao
  has_many :recurso, through: :perfil_recurso

  # validações ------------------------------------------------------------------------------------
  validates :descricao, :nome, :identificacao_interna, presence: true
  validates :nome, presence: true, uniqueness: { case_sensitive: false }
  validates :identificacao_interna, presence: true, uniqueness: { case_sensitive: false }
end