class User < ActiveRecord::Base
  include SearchModule
  
  devise :confirmable, :lockable, :timeoutable, :omniauthable,
          :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Callbacks
  after_create :notificar_equipe, :gerar_estrutura

  # Relacionamentos
  has_many :authentication, dependent: :delete_all
  
  # Validacoes
  #validates :perfil_id, presence: true, if: Proc.new{ |user| user.new_record? }

  def self.from_omniauth(auth, current_user)
    authentication = Authentication.where(provider: auth.provider, uid: auth.uid.to_s).first_or_initialize
    if authentication.user.blank?
      user = current_user.nil? ? User.where('email = ?', auth['info']['email']).first : current_user
      if user.blank?
        user = User.new
        user.confirmed_at, user.confirmation_sent_at = Time.now
        user.email = auth.info.email
        # Set a random password for omniauthenticated users
        user.password, user.password_confirmation = Devise.friendly_token

        user.save

        if user.persisted?
          authentication.user_id = user.id
          authentication.save
        else
          # se nao foi persistido pode ser porque nao foi fornecido o email pelo provider
          # entao retiramos a confirmacao para evitar o acesso direto com qualquer email
          # preenchido na tela de novo cadastro para onde sera direcionado daqui pelo omniauth_callbacks_controller
          user.confirmed_at, user.confirmation_sent_at = nil
          user.password, user.password_confirmation = nil
        end
      end
        
      user
    else
      authentication.user
    end
  end

  def self.new_with_session(params, session)
    if session['devise.user_attributes']
      new(session['devise.user_attributes'], without_protection: true) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end

  def update_with_password(params={})
    current_password = params.delete(:current_password)
    check_password = true
    if params[:password].blank?
      params.delete(:password)
      if params[:password_confirmation].blank?
        params.delete(:password_confirmation)
        check_password = false
      end
    end
    result = if valid_password?(current_password) || !check_password
               update_attributes(params)
             else
               self.errors.add(:current_password, current_password.blank? ? :blank : :invalid)
               self.attributes = params
               false
             end
    clean_up_passwords
    result
  end

  private
    def notificar_equipe
      #MailNotifier.delay.novo_user(self)
    end
    
    def gerar_estrutura
      
    end
    
    def criar_pessoa      
    end
end