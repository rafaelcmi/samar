class Endereco < ActiveRecord::Base
  belongs_to :enderecavel, polymorphic: true

  validates :bairro, :cep, :cidade, :destinatario, :endereco, :numero, :uf, presence: true
end