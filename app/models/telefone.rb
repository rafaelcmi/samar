class Telefone < ActiveRecord::Base
  # Relacionamentos
  belongs_to :pais
  belongs_to :telefonavel, polymorphic: true
  belongs_to :telefone_ddd
  belongs_to :telefone_tipo

  # Validacoes
  # nao pode usar :telefonavel_id, :telefonavel_type, porque tem accepts_nested_attributes_for em pessoa
  validates :numero, :pais_id, :telefone_ddd_id, :telefone_tipo_id, presence: true
  validates :numero, uniqueness: {
    case_sensitive: false,
    scope: [
      :pais_id,
      :telefone_ddd_id,
      :telefone_tipo_id
    ]
  }
end