# representa as actions dos controllers
class Acao < ActiveRecord::Base
  include SearchModule

  # relacionamentos -----------------------------------------------------------
  has_many :recurso, dependent: :restrict_with_error

  # validacoes ----------------------------------------------------------------
  validates :nome, presence: true, uniqueness: { case_sensitive: false }
end