# representa os recursos (controller#action) do sistema
class Recurso < ActiveRecord::Base
  # will_paginate
  self.per_page = 1000

  # relacionamentos -------------------------------------------------------------------------------
  belongs_to :acao
  belongs_to :controlador
  belongs_to :recurso_categoria
  has_many :erro, dependent: :restrict_with_error
  has_many :perfil, through: :perfil_recurso
  has_many :perfil_recurso, dependent: :delete_all

  # scopes ----------------------------------------------------------------------------------------
  # override por causa dos includes
  scope :listar, lambda { |search, order = 'visivel DESC, recurso_categoria_nome, nome'|
    select('recursos.*, recursos_categorias.nome AS recurso_categoria_nome')
    .where('recursos.nome LIKE ?', "%#{ search }%")
    .joins(:recurso_categoria)
    .includes([:acao, :controlador])
    .order(order)
  }

  scope :visivel, -> { where(visivel: true).order(:nome) }

  # validações ------------------------------------------------------------------------------------
  validates :acao_id, :controlador_id, :recurso_categoria_id, presence: true
  validates :acao_id, uniqueness: { scope: :controlador_id }
  validates :controlador_id, uniqueness: { scope: :acao_id }
  validates :nome, presence: true, uniqueness: { case_sensitive: false, scope: :recurso_categoria_id }
  validates :recurso_categoria_id, presence: true, uniqueness: { case_sensitive: false, scope: :nome }
end