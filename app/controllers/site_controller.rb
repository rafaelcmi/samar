class SiteController < ApplicationController
  skip_before_filter :authenticate_user!
  skip_before_filter :tem_permissao?

  def index
    if user_signed_in?
      # para que a msg de login com sucesso va para a view condos_path
      # unless flash[:notice].nil?
        # flash[:notice] = flash[:notice]
      # end

      # Ao usuario criar seu cadastro no sistema, todas os registros de
      # pessoas com o e-mail igual ao cadastrado como usuario devem ser
      # associadas a esse usuario, possibilitando assim chegar às
      # participacoes cadastradas com o e-mail em questao. Entao, sempre
      # no login esta verificacao serah feita para garantir a regra
      
      #Pessoa.where(email: current_user.email, user_id: nil).each do |p|
      #  p.user = current_user

      #  if p.may_aceitar?
      #    p.aceitar
      #  end

      #  p.save
      #end
      if (current_user.admin?)
        redirect_to acoes_path
      else
        redirect_to users_path + '/edit'
      end    
    end
  end
end