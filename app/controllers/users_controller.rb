class UsersController < ApplicationController
  helper_method :sort_column, :sort_direction
  
  skip_before_action :tem_permissao?
  before_action :user_administrador_sistema?
  before_action :resetar_session

  def index
    @users = User.listar(params[:search], sort_column + ' ' + sort_direction)
    @users = @users.page(params[:page]) unless params[:format].present?

    respond_to do |format|
      format.html
      format.pdf {
        pdf = UsersPdf.new(User.listar(nil, 'current_sign_in_at DESC'), User)
        send_data pdf.render, filename: "#{ t('activerecord.models.user.other') }.pdf", disposition: 'inline'
      }
      format.xlsx {
        response.headers['Content-Disposition'] = "attachment; filename=#{ t('activerecord.models.user.other') }.xlsx"
      }
    end
  end

  private
    def sort_column
      User.column_names.include?(params[:sort]) ? params[:sort] : 'current_sign_in_at'
    end

    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : 'desc'
    end
end
