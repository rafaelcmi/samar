class RecursosController < ApplicationController
  helper_method :sort_column, :sort_direction
  
  skip_before_action :tem_permissao?
  before_action :user_administrador_sistema?
  before_action :set_recurso, only: [:edit, :update, :destroy]
  before_action :resetar_session
  
  # breadcrumb ------------------------------------------------------------------------------------
  before_action ->(texto=t('activerecord.models.recurso.other'), url=recursos_path) {
    add_crumb(texto, url) }, except: [:index, :destroy]

  before_action ->(texto=t('views.new.titulo', model: Recurso.model_name.human),
                    url=new_recurso_path) {
    add_crumb(texto, url) }, only: [:new, :create]

  before_action ->(texto=t('views.edit.titulo', model: Recurso.model_name.human),
                    url=edit_recurso_path(@recurso)) {
    add_crumb(texto, url) }, only: [:edit, :update]

  # actions ---------------------------------------------------------------------------------------
  def index
    @recursos = Recurso.listar(params[:search], sort_column + ' ' + sort_direction).page(params[:page])
  end

  def new
    @recurso = Recurso.new
  end

  def edit
  end

  def create
    @recurso = Recurso.new(recurso_params)

    if @recurso.save
      redirect_to recursos_url, notice: t('mensagens.flash.create', crud: Recurso.model_name.human)
    else
      render :new
    end
  end

  def update
    if @recurso.update_attributes(recurso_params)
      redirect_to recursos_url, notice: t('mensagens.flash.update', crud: Recurso.model_name.human)
    else
      render :edit
    end
  end

  def destroy
    if @recurso.destroy
      redirect_to recursos_url, notice: t('mensagens.flash.destroy', crud: Recurso.model_name.human)
    else
      redirect_to recursos_url, alert: @recurso.errors.messages[:base][0]
    end
  end

  private
    def set_recurso
      @recurso = Recurso.find(params[:id])
    end

    def recurso_params
      params.require(:recurso).permit(
        :acao_id,
        :controlador_id,
        :nome,
        :recurso_categoria_id,
        :visivel
        )
    end
    
    def sort_column
      Recurso.column_names.include?(params[:sort]) ? params[:sort] : 'nome'
    end    
end