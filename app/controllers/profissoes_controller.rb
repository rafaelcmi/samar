class ProfissoesController < ApplicationController
  helper_method :sort_column, :sort_direction  

  skip_before_action :tem_permissao?
  before_action :user_administrador_sistema?
  before_action :set_profissao, only: [:edit, :update, :destroy]
  before_action :resetar_session

  # breadcrumb ------------------------------------------------------------------------------------
  before_action ->(texto=t('activerecord.models.profissao.other'), url=profissoes_path) {
    add_crumb(texto, url) }, except: [:index, :destroy]

  before_action ->(texto=t('views.new.titulo', model: Profissao.model_name.human),
                    url=new_profissao_path) {
    add_crumb(texto, url) }, only: [:new, :create]

  before_action ->(texto=t('views.edit.titulo', model: Profissao.model_name.human),
                    url=edit_profissao_path(@profissao)) {
    add_crumb(texto, url) }, only: [:edit, :update]

  def index
    @profissoes = Profissao.listar(params[:search], sort_column + ' ' + sort_direction).page(params[:page])
  end

  def new
    @profissao = Profissao.new
  end

  def edit
  end

  def create
    @profissao = Profissao.new(profissao_params)
    if @profissao.save
      redirect_to profissoes_url, notice: t('mensagens.flash.create', crud: Profissao.model_name.human)
    else
      render :new  
    end
  end

  def update
    if @profissao.update(profissao_params)
      redirect_to profissoes_url, notice: t('mensagens.flash.update', crud: Profissao.model_name.human)
    else
      render :edit
    end
  end

  def destroy
    if @profissao.destroy
      redirect_to profissoes_url, notice: t('mensagens.flash.destroy', crud: Profissao.model_name.human)
    else
      redirect_to profissoes_url, alert: @profissao.errors.messages[:base][0]
    end
  end

  private
    def set_profissao
      @profissao = Profissao.find(params[:id])
    end

    def profissao_params
      params.require(:profissao).permit(:nome)
    end
    
    def sort_column
      Profissao.column_names.include?(params[:sort]) ? params[:sort] : 'nome'
    end
end