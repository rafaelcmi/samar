class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  # captura all erros, essa é a classe pai de all erros
  rescue_from StandardError, with: :erro if Rails.env == 'production'

  before_action :authenticate_user! # method do Devise que redireciona para login se não está logado
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :tem_permissao?, except: :erro

  protected
    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(
        :email, :password, :password_confirmation, :current_password) }

      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(
        :email, :password, :password_confirmation) }
    end

  private
    # Para utilizacao deste method e necessario sobrescrever os methods incluir_associacao e remover_associacao
    def atualizar_associacoes(ids_novos, ids_atuais)
      lista_ids_incluir = (ids_novos - ids_atuais) - ["", nil]
      lista_ids_excluir = (ids_atuais - ids_novos) - ["", nil]
      
=begin
        # puts "\n\n*************\n\n"
        # puts "Elementos Novos = " + ids_novos.to_s
        # puts "\n"
        # puts "Elementos Atuais = " + ids_atuais.to_s
        # puts "\n"
        # puts "Elementos a Incluir = " + lista_ids_incluir.to_s
        # puts "\n"
        # puts "Elementos a Remover = " + lista_ids_excluir.to_s
        # puts "\n\n*************\n\n"
=end
      
      if (lista_ids_incluir != lista_ids_excluir)
        # Excluir elementos
        if (lista_ids_excluir && lista_ids_excluir.length > 0)
          lista_ids_excluir.each do |id_remocao|
            if !id_remocao.blank?
              remover_associacao(id_remocao)
            end
          end
        end
        
        # Incluir elementos
        if (lista_ids_incluir && lista_ids_incluir.length > 0)
          lista_ids_incluir.each do |id_inclusao|
            if !id_inclusao.blank?
              incluir_associacao(id_inclusao)
            end
          end
        end
      end
    end
    
    # Methods a serem sobrescritos
    def remover_associacao(id_remocao)
      raise 'Nenhum registro removido! Implemente os metodos remover_associacao e incluir_associacao'
    end
    
    def incluir_associacao(id_inclusao)
      raise 'Nenhum registro incluso! Implemente os metodos remover_associacao e incluir_associacao'
    end
    # ---------------------------------------------------------------------------------------------
    
    def breadcrumb
      add_crumb I18n.t('activerecord.models.empresa.other'), empresas_path
      add_crumb session[:empresa_nome], empresa_path(session[:empresa_id]) if session[:empresa_id]
    end

    def erro(exception = nil)
      logger.warn '##########################################################'
      logger.warn '1##### Entrou no erro'
      get_recurso()
      logger.warn "2##### Recurso: #{@recurso.controlador.nome} / #{@recurso.acao.nome}" rescue "recurso = erro"

      if exception.nil?
        nome_erro = I18n.t('mensagens.erros.sem_permissao')
        params.delete(:utf8) # estourou erro no MySQL com caracter especial
        descricao_erro = params
      else
        nome_erro = exception.class.to_s.gsub('::', ' :: ') # caso visualize por celular ter quebra de linha
        descricao_erro = exception.to_s
      end
      logger.warn "3##### Erro: #{nome_erro}" rescue 'erro = erro'
      logger.warn "4##### Descricao do erro: #{descricao_erro}" rescue 'descricao = erro'
      logger.warn "5##### Dispositivo: #{request.user_agent.to_s}" rescue 'dispositivo = erro'
      logger.warn "6##### empresa_id: #{session[:empresa_id]}" rescue 'empresa_id = erro'
      logger.warn "7##### ip: #{request.remote_ip}" rescue 'remote_id = erro'
      logger.warn "8##### user_id: #{current_user.id}" rescue 'user_id = erro'
      logger.warn "9##### user_email: #{current_user.email}" rescue 'user_email = erro'
      logger.warn '##########################################################'

      # para não aparecer sempre o erro genérico, ao ir aparecendo erros comuns
      # vamos mapeando para mostrar uma msg de erro mais direcionada.
      case nome_erro
        when 'ActionController :: RoutingError'
          session[:temp_msg] = I18n.t('mensagens.erros.pagina_nao_encontrada')
        when 'ActiveRecord :: RecordNotFound'
          if descricao_erro == "Couldn't find Empresa without an ID"
            flash[:alert] = I18n.t('mensagens.erros.selecione_empresa')
            redirect_to empresas_path
            return
          else
            session[:temp_msg] = I18n.t('mensagens.erros.registro_nao_encontrado')
          end
        when I18n.t('mensagens.erros.sem_permissao')
          session[:temp_msg] = I18n.t('mensagens.erros.sem_permissao')
        when 'ActiveRecord :: StatementInvalid'
          session[:temp_msg] = I18n.t('mensagens.erros.banco_dados')
        else
          session[:temp_msg] = I18n.t('mensagens.erros.generico')
      end

      # usando @erro ao invés de erro, porque é capturado no RSpec
      @erro = Erro.create(
      #  empresa_id: session[:empresa_id],
        descricao: descricao_erro,
        dispositivo: request.user_agent.to_s,
        ip: request.remote_ip,
        nome: nome_erro,
        recurso_id: @recurso.id,
        user_id: (current_user.id rescue nil)
      )

      #MailNotifier.delay.erro(@erro)

      redirect_to @erro
    end

    def find_privileges
      session[:privileges] ||= Privileges.new
    end
    helper_method :find_privileges

    def get_recurso
      acao   = Acao.find_by_nome(action_name)
      acao ||= Acao.create(nome: action_name)

      controlador   = Controlador.find_by_nome(self.class.controller_path)
      controlador ||= Controlador.create(nome: self.class.controller_path)

      recurso_categoria   = RecursoCategoria.first
      recurso_categoria ||= RecursoCategoria.create(nome: t('banco_dados.registro.geral'))

      @recurso   = Recurso.find_by_acao_id_and_controlador_id(acao.id, controlador.id)
      @recurso ||= Recurso.create(acao_id: acao.id,
      controlador_id: controlador.id,
      nome: "#{ controlador.nome } / #{ acao.nome }",
      recurso_categoria_id: recurso_categoria.id)
    end

    def resetar_session
      session[:administrador_empresa] = session[:empresa_id] = session[:empresa_nome] = nil
      #session[:pessoa_id] = session[:privileges] = session[:temp_msg] = nil
    end
    
    def set_empresa
      @empresa ||= Empresa.find(session[:empresa_id])
    end
    
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : 'asc'
    end    

    def tem_permissao?(param_action=action_name)
      privileges = find_privileges

      if self.class.controller_path.to_s[0..5] == 'devise' ||
        privileges.item_existe?(param_action, self.class.controller_path) ||
        #session[:administrador_empresa]
        true
      elsif session[:empresa_id].nil?
        flash[:alert] = I18n.t('mensagens.erros.selecione_empresa')
        #redirect_to empresas_path
        false
      else
        erro()
        false
      end
    end

    def user_administrador_sistema?
      unless current_user.admin?
        erro()
        false
      end
    end
end