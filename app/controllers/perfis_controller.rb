class PerfisController < ApplicationController
  helper_method :sort_column, :sort_direction  

  skip_before_action :tem_permissao?
  before_action :user_administrador_sistema?
  before_action :set_perfil, only: [:edit, :update, :destroy]
  before_action :resetar_session

  # breadcrumb ------------------------------------------------------------------------------------
  before_action ->(texto=t('activerecord.models.perfil.other'), url=perfis_path) {
    add_crumb(texto, url) }, except: [:index, :destroy]

  before_action ->(texto=t('views.new.titulo', model: Perfil.model_name.human),
                    url=new_perfil_path) {
    add_crumb(texto, url) }, only: [:new, :create]

  before_action ->(texto=t('views.edit.titulo', model: Perfil.model_name.human),
                    url=edit_perfil_path(@perfil)) {
    add_crumb(texto, url) }, only: [:edit, :update]

  # actions ---------------------------------------------------------------------------------------
  def index
    @perfis = Perfil.listar(params[:search], sort_column + ' ' + sort_direction).page(params[:page])
    @recursos = Recurso.visivel unless params[:format].nil?

    respond_to do |format|
      format.html
      format.xls
    end
  end

  def new
    @perfil = Perfil.new
  end

  def edit
    
  end

  def create
    @perfil = Perfil.new(perfil_params)

    if @perfil.save
      vincular_recursos
      redirect_to perfis_url, notice: t('mensagens.flash.create', crud: Perfil.model_name.human)
    else
      render :new
    end
  end

  #TODO: comparar com o controller de pessoas para ser feito igual a comparacao de params[:check_recurso]
  def update
    if @perfil.update_attributes(perfil_params)
      if params[:check_recurso].nil?
        params_tela = []
      else
        params_tela = params[:check_recurso].values.collect{|i| i.to_i}
      end
      atualizar_associacoes(params_tela, @perfil.perfil_recurso.pluck(:recurso_id))
      redirect_to perfis_url, notice: t('mensagens.flash.update', crud: Perfil.model_name.human)
    else
      render :edit
    end
  end

  def vincular_recursos
    if !params[:check_recurso].nil?
      params[:check_recurso].values.each do |id_recurso|
        if !id_recurso.blank?
          @perfil_recurso = PerfilRecurso.new
          @perfil_recurso.perfil_id = @perfil.id
          @perfil_recurso.recurso_id = id_recurso
          @perfil_recurso.save
        end
      end
    end    
  end

  def destroy
    @perfil.destroy
    redirect_to perfis_url, notice: t('mensagens.flash.destroy', crud: Perfil.model_name.human)
  end

  private
    def remover_associacao(id_remocao)
      @perfil.perfil_recurso.each do |perfil_recurso|
        if (id_remocao.to_i == perfil_recurso.recurso_id)
          PerfilRecurso.delete_all(['perfil_id = ? AND recurso_id = ?', perfil_recurso.perfil_id, id_remocao])
        end
      end
    end

    def incluir_associacao(id_inclusao)
      @perfil_recurso = PerfilRecurso.new
      @perfil_recurso.perfil_id = @perfil.id
      @perfil_recurso.recurso_id = id_inclusao
      @perfil_recurso.save
    end

    def perfil_params
      if action_name == 'update'
        params.require(:perfil).permit(:descricao, :ativacao_automatica, :identificacao_interna)
      else
        params.require(:perfil).permit(:nome, :descricao, :ativacao_automatica, :identificacao_interna)
      end
    end

    def set_perfil
      @perfil = Perfil.find(params[:id])
    end
    
     def sort_column
      Perfil.column_names.include?(params[:sort]) ? params[:sort] : 'nome'
    end 
end