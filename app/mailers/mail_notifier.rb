class MailNotifier < ActionMailer::Base
  default from: Rails.application.secrets.mailer_sender
  default reply_to: Rails.application.secrets.mailer_from

  # methods ---------------------------------------------------------------------------------------
  def contato(contato)
    @contato  = contato

    mail(
      subject: I18n.t('mail_notifier.contato.subject'),
      to: Rails.application.secrets.mailer_from
     )
  end

  def convite(empresa, autor, convidado)
    @empresa = empresa
    @autor = autor
    @convidado  = convidado

    mail(
      subject: I18n.t('mail_notifier.convite.subject'),
      to: @convidado.email
     )
  end

  def convite_aceito_recusado(status, empresa, usuario)
    @empresa = empresa.nome
    @usuario = "#{usuario.name} (#{usuario.email})"
    @status  = status

    if empresa.admin_id == empresa.gestor_id
      para = empresa.admin.email
    else
      para = empresa.admin.email, empresa.gestor.email
    end

    mail(
      subject: I18n.t('mail_notifier.convite_aceito_recusado.subject', status: @status, usuario: @usuario),
      to: para
     )
  end

  def convite_excluido(empresa, usuario_excluido, usuario_logado)
    @empresa = empresa.nome
    @usuario = "#{usuario_logado.name} (#{usuario_logado.email})"

    mail(
      subject: I18n.t('mail_notifier.convite_excluido.subject', empresa: @empresa),
      to: usuario_excluido.email
     )
  end

  def erro(erro)
    @erro    = erro
    @usuario = erro.user.email rescue '-'

    mail(
      subject: I18n.t('mail_notifier.erro.subject', erro: @erro.nome),
      to: Rails.application.secrets.mailer_from
    )
  end

  def nova_empresa(empresa, user_email)
    @empresa = empresa
    @user_email = user_email

    mail(
      subject: I18n.t('mail_notifier.nova_empresa.subject', nome: @empresa.nome),
      to: Rails.application.secrets.mailer_from
    )
  end

  def novo_user(user)
    @user = user

    mail(
      subject: I18n.t('mail_notifier.novo_user.subject', email: @user.email),
      to: Rails.application.secrets.mailer_from
    )
  end

  def pessoa_perfil_alterado(pessoa, empresa)
    @pessoa = pessoa
    @empresa = empresa

    mail(
      subject: I18n.t('mail_notifier.pessoa_perfil_alterado.subject', empresa_nome: @empresa.nome),
      to: pessoa.email
    )
  end
end