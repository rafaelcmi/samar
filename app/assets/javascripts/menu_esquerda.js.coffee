jQuery ->
  if $('#administracao li').length == 0
    $('#administracao').hide()
  
  if $('#cadastros li').length == 0
    $('#cadastros').hide()
    
  # Administração ---------------------------------------------------------------------------------
  $('#administracao').on('show.bs.collapse', ->
    if $('#administracao').find('i').hasClass('fa fa-caret-right')
        $('#administracao').find('.panel-heading').find('i').removeClass('fa fa-caret-right').addClass('fa fa-caret-down')
  )
    
  $('#administracao').on('hide.bs.collapse', ->
    if $('#administracao').find('i').hasClass('fa fa-caret-down')
      $('#administracao').find('.panel-heading').find('i').removeClass('fa fa-caret-down').addClass('fa fa-caret-right')
  )
  
  if $('#collapse_administracao').hasClass('panel-collapse collapse in')
    if $('#administracao').find('i').hasClass('fa fa-caret-right')
      $('#administracao').find('.panel-heading').find('i').removeClass('fa fa-caret-right').addClass('fa fa-caret-down')
  else
    if $('#administracao').find('i').hasClass('fa fa-caret-down')
      $('#administracao').find('.panel-heading').find('i').removeClass('fa fa-caret-down').addClass('fa fa-caret-right')

  # Cadastros -------------------------------------------------------------------------------------
  $('#cadastros').on('show.bs.collapse', ->
    if $('#cadastros').find('i').hasClass('fa fa-caret-right')
      $('#cadastros').find('.panel-heading').find('i').removeClass('fa fa-caret-right').addClass('fa fa-caret-down')
  )
    
  $('#cadastros').on('hide.bs.collapse', ->
    if $('#cadastros').find('i').hasClass('fa fa-caret-down')
      $('#cadastros').find('.panel-heading').find('i').removeClass('fa fa-caret-down').addClass('fa fa-caret-right')
  )
  
  if $('#collapse_cadastros').hasClass('panel-collapse collapse in')
    if $('#cadastros').find('i').hasClass('fa fa-caret-right')
      $('#cadastros').find('.panel-heading').find('i').removeClass('fa fa-caret-right').addClass('fa fa-caret-down')
  else
    if $('#cadastros').find('i').hasClass('fa fa-caret-down')
      $('#cadastros').find('.panel-heading').find('i').removeClass('fa fa-caret-down').addClass('fa fa-caret-right')