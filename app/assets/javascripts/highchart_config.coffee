Highcharts.setOptions
  lang:
    contextButtonTitle: "Menu do gráfico"
    decimalPoint: ","
    downloadJPEG: 'Download JPEG'
    downloadPDF: 'Download PDF'
    downloadPNG: 'Download PNG'
    downloadSVG: 'Download SVG'
    loading: 'Carregando ...'
    noData: 'Sem dados para serem exibidos'
    printChart: 'Imprimir o gráfico'
    thousandsSep: '.'
