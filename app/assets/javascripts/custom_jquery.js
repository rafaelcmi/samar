/* Accordion ----------------------------------------------------------------------------------- */
$(function() {
    $(".accordion").accordion({
        collapsible: true
    });
});

/* InputMask ----------------------------------------------------------------------------------- */
$(function() {
    $(".inputmask_cep").inputmask("99999-999");
});

$(function() {
    $(".inputmask_celular").inputmask("(99) 99999-9999");
});

$(function() {
    $(".inputmask_residencial").inputmask("(99) 9999-9999");
});

$(function() {
    $(".inputmask_cnpj").inputmask("99.999.999/9999-99");
});

$(function() {
    $(".inputmask_cpf").inputmask("999.999.999-99");
});

$(function() {
    $(".inputmask_data").inputmask("d/m/y", { "placeholder": "dd/mm/aaaa" });
});

/* DatePicker ---------------------------------------------------------------------------------- */
$(function() {
    $(".datepicker").datepicker({
        showOn: "button",
        showOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        selectOtherMonths: true,
        dayNamesMin: ['Do','Seg','Ter','Qua','Qui','Sex','Sáb'],
        buttonText: "<i class='fa fa-calendar fa-lg'></i>"
    });
});

/* Mostrar/Esconder menu quando for por celular ------------------------------------------------ */
$(function() {
    $("#responsive-menu-button").sidr({
        name: "sidr-main",
        source: "#navigation"
    });
});

/* Aplicar mascara e estilo */
function datepicker_inputmask(){
	$(".datepicker").datepicker({
        showOn: "button",
        showOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        selectOtherMonths: true,
        dayNamesMin: ['Do','Seg','Ter','Qua','Qui','Sex','Sáb'],
        buttonText: "<i class='fa fa-calendar fa-lg'></i>"
    });
	
	$(".inputmask_data").inputmask("d/m/y", {"placeholder": "dd/mm/aaaa"});
}