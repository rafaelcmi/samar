//= require jquery
//= require jquery_ujs

// require jquery-ui
//= require jquery-ui/accordion
// require jquery-ui/autocomplete
// require jquery-ui/button
//= require jquery-ui/datepicker
//= require jquery-ui/datepicker-pt-BR
// require jquery-ui/dialog
// require jquery-ui/menu
// require jquery-ui/progressbar
// require jquery-ui/selectmenu
// require jquery-ui/slider
// require jquery-ui/spinner
// require jquery-ui/tabs
// require jquery-ui/tooltip

// estao na pasta vendor
//= require jquery.inputmask
//= require jquery.inputmask.date.extensions

// menu lateral para mobile
// esta na pasta vendor
//= require jquery.sidr

// require bootstrap/affix
//= require bootstrap/alert
// require bootstrap/button
// require bootstrap/carousel
//= require bootstrap/collapse
//= require bootstrap/dropdown
//= require bootstrap/tab
// require bootstrap/transition
// require bootstrap/scrollspy
// require bootstrap/modal
// require bootstrap/tooltip
// require bootstrap/popover
// require bootstrap-sprockets

// estao na pasta vendor
//= require highcharts/highcharts
//= require highcharts/highcharts-more
//= require highcharts/modules/exporting

// customizado para portugues
//= require highchart_config

//= require custom_jquery

// esse eh necessario por causa da gem nested_form_fields
//= require nested_form_fields

//= require buscar_cep
//= require menu_esquerda