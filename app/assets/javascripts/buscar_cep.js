function carregaEndereco(input) {
    if(input.value != '' && !input.value.match(/^[0-9]{5}[-]?[0-9]{3}$/)) {
        alert("O CEP informado não está no formato válido.");
    }
    else if(input.value != '') {
        // $("#endereco_cep_label").html(" Pesquisando...");
        $("#searching").css('display','block');
        $.getJSON("/empresas/busca_por_cep/" + input.value, function (data) {
            if(data['erro']){
                alert(data['erro']);
                $('#empresa_endereco').focus();
            }
            else{
                //$('#condo_numero').focus();
                // com $("[id$=_endereco]") ele faz uma busca com um id com final _endereco
                // e assim posso usar esse método para qualquer tela que busque um cep

                // Webservice esta retornando esta estrutura:
                //0: "<<logradouro>>"
                //1: "<<endereco>>"
                //2: "<<bairro>"
                //3: "<<cidade>>"
                //4: "<<estado>>"
                //5: "<<cep>>"
                $("[id$=_endereco]").val(data[0] + " " + data[1]);
                $("[id$=_bairro]").val(data[2]);
                $("[id$=_cidade]").val(data[3]);
                $("[id$=_uf]").val(data[4]);
            }
            $("#searching").css('display','none');
            // $("#endereco_cep_label").html(" CEP");
        });
    }
}
