module NavigationHelper
  def currently_at(tab, posicao=0)
    render partial: 'layouts/main_nav', locals: { current_tab: tab }
    render partial: 'layouts/menu_esquerda', locals: { current_tab: tab, posicao_accordion: posicao }
  end

  # retornar :class "active" ou "inactive" dependendo do valor passado por parâmetro
  def nav_tab(title, url, options = {})
    current_tab = options.delete(:current)
    
    titulo = options.delete(:titulo) unless options[:titulo].nil?
    titulo ||= title

    # quando foi usado o excluir empresa com uma view destroy para confirmar, o item do menu eh do method :delete
    # e assim se quiser passar qualquer method diferente de get eh soh colocar nas options
    method = options.delete(:method) unless options[:method].nil?
    metodo ||= :get

    # quando estamos navegando pelo celular, ao clicar em algum item do menu esquerdo ele ira posicionar num
    # link que foi criado no helper titulo e que se chama 'i', e se colocar no menu esquerdo a opcao inicio=true
    # ele colocara no fim da url o valor #i, por exemplo, /perfis#i
    inicio = '#i' unless options[:inicio].nil?
    inicio ||= ''

    options[:class] = (current_tab == titulo ? 'active' : 'inactive')
    # é colocado o id do link para ser usado nas features do RSpec
    content_tag(:li, link_to(title, url + inicio, id: url, method: method), options)
  end
end