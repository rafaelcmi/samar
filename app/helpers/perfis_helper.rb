module PerfisHelper
  def exibir_recursos
    @recursos_categorias = []

    RecursoCategoria.order(:nome).each do |recurso_categoria|
      recursos_visiveis = recurso_categoria.recurso.visivel
      next if recursos_visiveis.size == 0

      recursos = []

      recursos_visiveis.each do |recurso|
        encontrou = false

        @perfil.perfil_recurso.each do |perfil_recurso|
          if perfil_recurso.recurso_id == recurso.id
            perfil_recurso.nome = recurso.nome
            perfil_recurso.selecionado = true
            recursos.push(perfil_recurso)

            encontrou = true
            break
          end
        end
        
        if (!encontrou)
          @perfil_recurso = PerfilRecurso.new
          @perfil_recurso.nome = recurso.nome
          @perfil_recurso.perfil_id = @perfil.id
          @perfil_recurso.recurso_id = recurso.id

          recursos.push(@perfil_recurso)
        end

        # recursos = recursos.paginate(page: params[:page])
      end
      
      @recurso_categoria = RecursoCategoria.new
      @recurso_categoria.id = recurso_categoria.id
      @recurso_categoria.nome = recurso_categoria.nome
      @recurso_categoria.recursos = recursos

      @recursos_categorias.push(@recurso_categoria)
    end
  end
end