class AddNomeIdentificadorToPerfil < ActiveRecord::Migration
  def change
    add_column :perfis, :identificacao_interna, :string, null: true
    
    execute 'UPDATE perfis SET identificacao_interna = nome;'
    
    change_column :perfis, :identificacao_interna, :string, null: false
  end
  
  def down
    remove_column :perfis, :identificacao_interna    
  end
end