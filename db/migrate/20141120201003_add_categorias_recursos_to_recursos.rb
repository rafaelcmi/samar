class AddCategoriasRecursosToRecursos < ActiveRecord::Migration
  def change
    add_column :recursos, :recurso_categoria_id, :integer

    add_foreign_key :recursos, :recursos_categorias
  end
  
  def down
    remove_column :recursos, :recurso_categoria_id
  end
end