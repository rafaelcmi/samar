class CreateTelefones < ActiveRecord::Migration
  def change
    create_table :telefones do |t|
      t.string :numero, null: false
      t.string :ramal
      t.references :telefonavel, polymorphic: true, index: true, null: false
      t.references :telefone_tipo, null: false
      t.references :telefone_ddd, null: false
      t.references :pais, null: false

      t.timestamps
    end

    add_index :telefones, [:numero, :telefonavel_type, :telefonavel_id, :telefone_tipo_id, :telefone_ddd_id, :pais_id], 
      unique: true, name: 'index_telefones_on_numero_and_telefonavel'
    add_index :telefones, [:telefonavel_type, :telefonavel_id]
    
    add_foreign_key :telefones, :telefones_tipos
    add_foreign_key :telefones, :telefones_ddds
    add_foreign_key :telefones, :paises
  end
end