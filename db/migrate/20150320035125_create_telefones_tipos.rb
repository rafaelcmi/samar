class CreateTelefonesTipos < ActiveRecord::Migration
  def change
    create_table :telefones_tipos do |t|
      t.string :nome, null: false
    end

    add_index :telefones_tipos, :nome, unique: true
  end
end