class CreatePerfis < ActiveRecord::Migration
  def up
    create_table :perfis do |t|
      t.string :nome, :null => false
      t.text :descricao
    end
    
     add_index :perfis, :nome, :unique => true   
  end
  
  def down
    drop_table :perfis
  end
end