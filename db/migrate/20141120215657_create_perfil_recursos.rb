class CreatePerfilRecursos < ActiveRecord::Migration
  def change
    create_table :perfis_recursos do |t|
      t.references :perfil, :null => false
      t.references :recurso, :null => false
    end
    
    add_index :perfis_recursos, [:perfil_id, :recurso_id], unique: true

    add_foreign_key :perfis_recursos, :perfis
    add_foreign_key :perfis_recursos, :recursos
  end
  
  def down
    drop_table :perfis_recursos
  end
end