class AddVisivelToRecurso < ActiveRecord::Migration
  def change
    add_column :recursos, :visivel, :boolean, :null => false, :default => false
  end
  
  def down
    remove_column :recursos, :visivel
  end
end
