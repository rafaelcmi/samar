require 'csv'
class CargaInicialTelefonesDdds < ActiveRecord::Migration
  def up
    arquivo_csv = File.read("#{Rails.root}/db/carga/telefones_ddds.csv")
    csv = CSV.parse(arquivo_csv, headers: true)
    csv.each do |row|
      TelefoneDdd.create!(row.to_hash)
    end
  end

  def down
    TelefoneDdd.delete_all
  end
end