class CreateEnderecos < ActiveRecord::Migration
  def change
    create_table :enderecos do |t|
      t.string :nome, null: false
      t.string :destinatario, null: false
      t.string :cep, null: false
      t.string :endereco, null: false
      t.string :numero, null: false
      t.string :complemento
      t.string :bairro, null: false
      t.string :cidade, null: false
      t.string :uf, null: false, limit: 2
      t.boolean :correspondencia, null: false, default: false
      t.references :enderecavel, polymorphic: true, null: false

      t.timestamps
    end

    add_index :enderecos, [:nome, :enderecavel_type, :enderecavel_id], 
      unique: true, name: 'index_enderecos_on_nome_and_enderecavel'
    add_index :enderecos, [:enderecavel_type, :enderecavel_id]
  end
end