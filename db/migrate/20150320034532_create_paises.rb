class CreatePaises < ActiveRecord::Migration
  def change
    create_table :paises do |t|
      t.string :nome, null: false
      t.string :ddi, null: false
    end
    
    add_index :paises, :nome, unique: true    
  end
end