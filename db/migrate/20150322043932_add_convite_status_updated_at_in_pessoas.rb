class AddConviteStatusUpdatedAtInPessoas < ActiveRecord::Migration
  def up
    add_column :pessoas, :convite_status_updated_at, :datetime, null: true

    execute 'UPDATE pessoas SET convite_status_updated_at = updated_at;'

    change_column :pessoas, :convite_status_updated_at, :datetime, null: false    
  end
  
  def down
    remove_column :pessoas, :convite_status_updated_at
  end
end