class CreateRecursos < ActiveRecord::Migration
  def up
    create_table :recursos do |t|
      t.references :acao, :null => false
      t.references :controlador, :null => false
      t.string :nome, :null => true
      t.timestamps
    end
    
    add_index :recursos, [:acao_id, :controlador_id], :unique => true
    add_index :recursos, :nome, :unique => true

    add_foreign_key :recursos, :acoes
    add_foreign_key :recursos, :controladores    
  end
  
  def down
    drop_table :recursos
  end
end