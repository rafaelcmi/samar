require 'csv'
class CargaInicialPaises < ActiveRecord::Migration
  def up
    arquivo_csv = File.read("#{Rails.root}/db/carga/paises.csv")
    csv = CSV.parse(arquivo_csv, headers: true)
    csv.each do |row|
      Pais.create!(row.to_hash)
    end
  end

  def down
    Pais.delete_all
  end
end