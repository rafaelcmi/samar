class CreateTelefonesDdds < ActiveRecord::Migration
  def change
    create_table :telefones_ddds do |t|
      t.string :codigo, null: false
    end

    add_index :telefones_ddds, :codigo, unique: true    
  end
end