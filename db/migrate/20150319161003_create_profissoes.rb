class CreateProfissoes < ActiveRecord::Migration
  def change
    create_table :profissoes do |t|
      t.string :nome, :null => false
      
      t.timestamps
    end
    
    add_index :profissoes, :nome, :unique => true    
  end
end