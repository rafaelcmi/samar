require 'csv'
class CargaInicialTelefonesTipos < ActiveRecord::Migration
  def up
    arquivo_csv = File.read("#{Rails.root}/db/carga/telefones_tipos.csv")
    csv = CSV.parse(arquivo_csv, headers: true)
    csv.each do |row|
      TelefoneTipo.create!(row.to_hash)
    end
  end

  def down
    TelefoneTipo.delete_all
  end
end