class AddAtivacaoAutomaticaToPerfil < ActiveRecord::Migration
  def change
    add_column :perfis, :ativacao_automatica, :boolean, null: false, default: true     
  end
  
  def down
    remove_column :perfis, :ativacao_automatica
  end
end