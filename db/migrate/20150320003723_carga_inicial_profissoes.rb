require 'csv'
class CargaInicialProfissoes < ActiveRecord::Migration
  def up
    arquivo_csv = File.read("#{Rails.root}/db/carga/profissoes.csv")
    csv = CSV.parse(arquivo_csv, headers: true)
    csv.each do |row|
      if Profissao.where(id: row['id']).blank?
        Profissao.create(row.to_hash)
      end
    end
  end

  def down
    Profissao.delete_all
  end
end