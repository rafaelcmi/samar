# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150530033208) do

  create_table "acoes", force: true do |t|
    t.string   "nome",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "acoes", ["nome"], name: "index_acoes_on_nome", unique: true, using: :btree

  create_table "contatos", force: true do |t|
    t.string   "nome"
    t.string   "email",      null: false
    t.string   "telefones"
    t.text     "mensagem",   null: false
    t.string   "ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "controladores", force: true do |t|
    t.string   "nome",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "controladores", ["nome"], name: "index_controladores_on_nome", unique: true, using: :btree

  create_table "enderecos", force: true do |t|
    t.string   "nome",                                       null: false
    t.string   "destinatario",                               null: false
    t.string   "cep",                                        null: false
    t.string   "endereco",                                   null: false
    t.string   "numero",                                     null: false
    t.string   "complemento"
    t.string   "bairro",                                     null: false
    t.string   "cidade",                                     null: false
    t.string   "uf",               limit: 2,                 null: false
    t.boolean  "correspondencia",            default: false, null: false
    t.integer  "enderecavel_id",                             null: false
    t.string   "enderecavel_type",                           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "enderecos", ["enderecavel_type", "enderecavel_id"], name: "index_enderecos_on_enderecavel_type_and_enderecavel_id", using: :btree
  add_index "enderecos", ["nome", "enderecavel_type", "enderecavel_id"], name: "index_enderecos_on_nome_and_enderecavel", unique: true, using: :btree

  create_table "erros", force: true do |t|
    t.string   "nome"
    t.text     "descricao"
    t.integer  "user_id"
    t.string   "ip"
    t.integer  "recurso_id",                 null: false
    t.text     "resolucao"
    t.integer  "erro_status_id", default: 1, null: false
    t.string   "dispositivo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "erros", ["erro_status_id"], name: "erros_erro_status_id_fk", using: :btree
  add_index "erros", ["recurso_id"], name: "erros_recurso_id_fk", using: :btree
  add_index "erros", ["user_id"], name: "erros_user_id_fk", using: :btree

  create_table "erros_status", force: true do |t|
    t.string "nome",         null: false
    t.string "classe",       null: false
    t.string "classe_badge", null: false
  end

  add_index "erros_status", ["nome"], name: "index_erros_status_on_nome", unique: true, using: :btree

  create_table "paises", force: true do |t|
    t.string "nome", null: false
    t.string "ddi",  null: false
  end

  add_index "paises", ["nome"], name: "index_paises_on_nome", unique: true, using: :btree

  create_table "perfis", force: true do |t|
    t.string  "nome",                                 null: false
    t.text    "descricao"
    t.boolean "ativacao_automatica",   default: true, null: false
    t.string  "identificacao_interna",                null: false
  end

  add_index "perfis", ["nome"], name: "index_perfis_on_nome", unique: true, using: :btree

  create_table "perfis_recursos", force: true do |t|
    t.integer "perfil_id",  null: false
    t.integer "recurso_id", null: false
  end

  add_index "perfis_recursos", ["perfil_id", "recurso_id"], name: "index_perfis_recursos_on_perfil_id_and_recurso_id", unique: true, using: :btree
  add_index "perfis_recursos", ["recurso_id"], name: "perfis_recursos_recurso_id_fk", using: :btree

  create_table "profissoes", force: true do |t|
    t.string   "nome",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "profissoes", ["nome"], name: "index_profissoes_on_nome", unique: true, using: :btree

  create_table "recursos", force: true do |t|
    t.integer  "acao_id",                              null: false
    t.integer  "controlador_id",                       null: false
    t.string   "nome"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "recurso_categoria_id"
    t.boolean  "visivel",              default: false, null: false
  end

  add_index "recursos", ["acao_id", "controlador_id"], name: "index_recursos_on_acao_id_and_controlador_id", unique: true, using: :btree
  add_index "recursos", ["controlador_id"], name: "recursos_controlador_id_fk", using: :btree
  add_index "recursos", ["nome"], name: "index_recursos_on_nome", unique: true, using: :btree
  add_index "recursos", ["recurso_categoria_id"], name: "recursos_recurso_categoria_id_fk", using: :btree

  create_table "recursos_categorias", force: true do |t|
    t.string   "nome",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "recursos_categorias", ["nome"], name: "index_recursos_categorias_on_nome", unique: true, using: :btree

  create_table "telefones", force: true do |t|
    t.string   "numero",           null: false
    t.string   "ramal"
    t.integer  "telefonavel_id",   null: false
    t.string   "telefonavel_type", null: false
    t.integer  "telefone_tipo_id", null: false
    t.integer  "telefone_ddd_id",  null: false
    t.integer  "pais_id",          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "telefones", ["numero", "telefonavel_type", "telefonavel_id", "telefone_tipo_id", "telefone_ddd_id", "pais_id"], name: "index_telefones_on_numero_and_telefonavel", unique: true, using: :btree
  add_index "telefones", ["pais_id"], name: "telefones_pais_id_fk", using: :btree
  add_index "telefones", ["telefonavel_id", "telefonavel_type"], name: "index_telefones_on_telefonavel_id_and_telefonavel_type", using: :btree
  add_index "telefones", ["telefonavel_type", "telefonavel_id"], name: "index_telefones_on_telefonavel_type_and_telefonavel_id", using: :btree
  add_index "telefones", ["telefone_ddd_id"], name: "telefones_telefone_ddd_id_fk", using: :btree
  add_index "telefones", ["telefone_tipo_id"], name: "telefones_telefone_tipo_id_fk", using: :btree

  create_table "telefones_ddds", force: true do |t|
    t.string "codigo", null: false
  end

  add_index "telefones_ddds", ["codigo"], name: "index_telefones_ddds_on_codigo", unique: true, using: :btree

  create_table "telefones_tipos", force: true do |t|
    t.string "nome", null: false
  end

  add_index "telefones_tipos", ["nome"], name: "index_telefones_tipos_on_nome", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.boolean  "admin",                  default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  add_foreign_key "erros", "erros_status", :name => "erros_erro_status_id_fk"
  add_foreign_key "erros", "recursos", :name => "erros_recurso_id_fk"
  add_foreign_key "erros", "users", :name => "erros_user_id_fk"

  add_foreign_key "perfis_recursos", "perfis", :name => "perfis_recursos_perfil_id_fk"
  add_foreign_key "perfis_recursos", "recursos", :name => "perfis_recursos_recurso_id_fk"

  add_foreign_key "recursos", "acoes", :name => "recursos_acao_id_fk"
  add_foreign_key "recursos", "controladores", :name => "recursos_controlador_id_fk"
  add_foreign_key "recursos", "recursos_categorias", :name => "recursos_recurso_categoria_id_fk"

  add_foreign_key "telefones", "paises", :name => "telefones_pais_id_fk"
  add_foreign_key "telefones", "telefones_ddds", :name => "telefones_telefone_ddd_id_fk"
  add_foreign_key "telefones", "telefones_tipos", :name => "telefones_telefone_tipo_id_fk"

end
