source 'https://rubygems.org'
ruby '2.1.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.8'

# Use mysql as the database for Active Record
gem 'mysql2'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'

# State Machine
gem 'aasm'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
#gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0 '
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# layout
gem 'bootstrap-sass', '~> 3.2.0'
gem 'autoprefixer-rails'
gem 'simple_form', '~> 3.1.0.rc2'#, github: 'plataformatec/simple_form'
# icons
gem 'font-awesome-sass', '~> 4.2.0'
# paginacao
gem 'bootstrap-will_paginate'
# form aninhado
gem 'nested_form_fields'

gem 'foreigner', '~> 1.4.1'

gem 'brcpfcnpj', '~> 3.3.0'
gem 'brcep', '~> 3.3.0'

# breadcrumb
gem 'crummy', '~> 1.8.0'

# localizacao para data e number
gem 'delocalize'

# graficos
gem 'lazy_high_charts'

# pdf
gem 'prawn', '~> 1.0.0.rc2'

# xls
gem 'axlsx'
gem 'axlsx_rails'

# seed
gem 'seed-fu', '~> 2.3'

# authentication
gem 'devise'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'omniauth-linkedin'
#gem 'omniauth-twitter'

gem 'rmagick','2.13.3'

# enviar e-mail em background
gem 'delayed_job_active_record'
gem 'daemons'
gem 'delayed_job_web'

gem 'tinymce-rails', '4.0.16' # Fixado por haver quebra de compatibilidade entre versões
gem 'tinymce-rails-langs', '4.20140129' # Fixado por haver quebra de compatibilidade entre versões

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring', group: :development
gem 'spring-commands-rspec', group: :development

group :development do
  # tela de erro interativa
  gem 'better_errors'
  gem 'binding_of_caller'
  # helps you kill all the N+1 queries, as well as unnecessarily eager loaded relations
  gem 'bullet'
  # Checks for vulnerable versions of gems in Gemfile.lock
  gem 'bundler-audit', require: false
  # It scans through your application and outputs a nicely formatted table of possible vulnerabilities.
  gem 'guard-brakeman'
  # abrir um preview do email
  gem 'letter_opener', '1.1.0'
  gem 'meta_request'
  gem 'quiet_assets'
  # Middleware that displays speed badge for every html page.
  gem 'rack-mini-profiler'
  # boas praticas de programacao
  gem 'rails_best_practices'

  gem 'rb-inotify', require: false
  gem 'rb-fsevent', require: false
  gem 'rb-fchange', require: false

  # Ruby static code analyzer
  gem 'rubocop', require: false
  # wraps around static analysis gems such as Reek, Flay and Flog to provide a quality report of your Ruby code
  gem 'rubycritic', require: false

  gem 'ruby_gntp'

  # verificar se tem rota sem controller#action e vice-versa
  gem 'traceroute'

  gem 'webrick', '1.3.1'
end

group :test do
  gem 'capybara'#, '~> 2.1.0'
  gem 'database_cleaner'
  gem 'email_spec'
  gem 'faker'
  gem 'launchy'#, '~> 2.3.0'
  # gem 'rack_session_access' get e set em session com capybara
  gem 'shoulda-matchers', require: false  
  gem 'simplecov', require: false
  gem 'simplecov-rcov', require: false
  gem 'ci_reporter', require: false
  gem 'ci_reporter_rspec', require: false
end

group :development, :test do
  gem 'factory_girl_rails'
  gem 'guard-rspec', require: false
  # gem 'growl_notify'
  gem 'rspec-rails'#, '~> 2.99'
end

# Deploy
gem 'capistrano', '3.3.5'
gem 'capistrano-bundler'
gem 'capistrano-rails'
gem 'capistrano-rbenv'