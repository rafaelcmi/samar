class CepFormatValidator < ActiveModel::EachValidator
  def validate_each(object, attribute, value)
    return if value.blank?

    unless value =~ /^[0-9]{5}[-]?[0-9]{3}$/
      object.errors.add(attribute, :invalid_format)
    end
  end
end