if defined? Bullet
  # Bullet.enable: enable Bullet gem, otherwise do nothing
  Bullet.enable = true

  # Bullet.alert: pop up a JavaScript alert in the browser
  Bullet.alert = true

  # Bullet.bullet_logger: log to the Bullet log file (Rails.root/log/bullet.log)
  Bullet.bullet_logger = true

  # Bullet.console: log warnings to your browser’s console.log (Safari/Webkit browsers or Firefox w/Firebug installed)
  Bullet.console = true

  # Bullet.growl: pop up Growl warnings if your system has Growl installed. Requires a little bit of configuration
  # Bullet.growl = true

  # Bullet.xmpp: send XMPP/Jabber notifications to the re  ceiver indicated. Note that the code will currently not handle the adding of contacts, so you will need to make both accounts indicated know each other manually before you will receive any notifications. If you restart the development server frequently, the ‘coming online’ sound for the bullet account may start to annoy – in this case set :show_online_status to false; you will still get notifications, but the bullet account won’t announce it’s online status anymore.
  #  Bullet.xmpp = { :account => 'bullets_account@jabber.org',
  #                  :password => 'bullets_password_for_jabber',
  #                  :receiver => 'your_account@jabber.org',
  #                  :show_online_status => true }

  # Bullet.rails_logger: add warnings directly to the Rails log
  #  Bullet.rails_logger = true

  # Bullet.airbrake: add notifications to airbrake
  #  Bullet.airbrake = true
end