Rails.application.routes.draw do

  root to: 'site#index'
  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }

  # TODO: tentar validar se o user eh admin
  authenticated :user do
    mount DelayedJobWeb, at: '/qazplm1029'
  end
  
  with_options except: :show do |except_show|
    except_show.resources :acoes
    except_show.resources :controladores
    except_show.resources :perfis
    except_show.resources :pessoas
    except_show.resources :recursos
    except_show.resources :recursos_categorias
    except_show.resources :profissoes    
  end
  
  resources :contatos, only: [:index, :new, :create, :destroy]
  resources :empresas
  resources :erros, except: [:new, :create]
  resources :users, only: :index

  get 'empresas/busca_por_cep/:cep' => 'empresas#busca_por_cep'
  get 'pessoas/:id/enviar_convite' => 'pessoas#enviar_convite', as: :enviar_convite
  post 'pessoa/filtrar' => 'pessoas#filtrar', as: :filtrar_elementos  
end