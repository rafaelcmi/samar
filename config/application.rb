require File.expand_path('../boot', __FILE__)

require 'rails/all'

# carrega um arquivo adicional que contem as senhas em variaveis de ambiente
ENV_PATH = File.expand_path('../env.rb', __FILE__)
require ENV_PATH if File.exists?(ENV_PATH)

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
#Bundler.require(*Rails.groups)
Bundler.require(:default, Rails.env)

module Samar
  class Application < Rails::Application
    config.autoload_paths += %W(#{config.root}/lib)

    config.time_zone = 'Brasilia'

    config.i18n.enforce_available_locales = false

    config.i18n.default_locale = :'pt-BR'

    config.relative_url_root = '/samar_homologacao' if Rails.env == 'production' && !ENV_PATH.scan('samar_homologacao').blank?


    config.action_mailer.default_url_options = { host: Rails.application.secrets.mailer_host }

    # don't generate RSpec tests for views and helpers
    config.generators do |g|
      g.fixture_replacement :factory_girl, dir: 'spec/factories'
      g.helper = false
      g.helper_specs = false
      g.javascripts = false
      g.request_specs = false
      g.stylesheets = false
      g.test_framework :rspec, fixture: true
      g.view_specs = false
    end
  end
end