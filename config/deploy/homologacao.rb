role :app, %w{deploy@samarrh.com.br}
role :web, %w{deploy@samarrh.com.br}
role :db,  %w{deploy@samarrh.com.br}

set :branch, 'develop'
set :deploy_to, '/home/deploy/apps/samarrh_homologacao'
set :repo_url, 'git@bitbucket_samar.org:samarrh/samar.git'

server 'samarrh.com.br', user: 'deploy', roles: %w{app}

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  before 'deploy:publishing', 'db:seed_fu'
  after :publishing, :restart
end