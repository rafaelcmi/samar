role :app, %w{deploy@samarrh.com.br}
role :web, %w{deploy@samarrh.com.br}
role :db,  %w{deploy@samarrh.com.br}

set :branch, 'master'
set :deploy_to, '/home/deploy/apps/samarrh_producao'
set :repo_url, 'git@bitbucket.org:samarrh/samar.git'

server 'condominiosimples.com.br', user: 'deploy', roles: %w{app}#, my_property: :my_value

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  desc 'Symlink para sistsamar e samar_homologacao'
  task :create_symlink do
    on roles(:app), in: :sequence, wait: 5 do
      execute 'ln -s ~/apps/redmine_2.3.1/public/ ~/apps/samarrh_producao/current/public/sistsamar'
      # execute 'ln -s ~/apps/condominiosimples_homologacao/current/public/ ~/apps/condominiosimples_producao/current/public/condominio_homologacao'
    end
  end

  before 'deploy:publishing', 'db:seed_fu'

  after :publishing, :restart
  after :restart, :create_symlink
end